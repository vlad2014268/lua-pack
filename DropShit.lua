script_name("DropShit")
script_author("THERION")
script_dependencies("SAMPFUNCS")

local RPC_GIVE_PLAYER_WEAPON = 22

local state = true
local drop_list = { 5, 8, 7, 2, 41 } 
-- baseball bat, katana, some stick, some other stick, spray

onReceiveRpc = function(packet_id, bs)
   if state and packet_id == RPC_GIVE_PLAYER_WEAPON then
      local weapon_id = raknetBitStreamReadInt32(bs)
      for _, i in ipairs(drop_list) do 
         if weapon_id == i then
            return false 
         end
      end
      -- if weapon is in list => drop it
   end
end

-- prints message to SAMP Chat
local function log(msg)
   local format = "{FB4343}[%s]{FFFFFF}: %s{FFFFFF}."
   sampAddChatMessage(string.format(format, thisScript().name, msg), -1)
end

function main()
   while not isSampAvailable() do wait(0) end

   sampRegisterChatCommand("dropshit", --switch the script with this command
   function()
      state = not state
      local state_txt = state and "{43FB43}ON" or "{FB4343}OFF"
      log("script is now " .. state_txt)
   end)

   wait(-1)
end
