script_name("FogDist")
script_authors("LUCHARE", "THERION")
script_dependencies("FixDIST")

-- prints message to SAMP Chat
local function log(msg)
   local format = "{FB4343}[%s]{FFFFFF}: %s{FFFFFF}."
   sampAddChatMessage(string.format(format, thisScript().name, msg), -1)
end

-- prints out if user does not have some of the needed files
local function catch_exception(status, path)
   local msg = string.format('File not found: "%s". Shutting down..', path)
   if not status then log(msg) end
   assert(status, msg)
end

do --Loading libraries
   local list = {
      ffi        = "ffi",
      inicfg     = "inicfg",
   }

   local result = nil
   for var, path in pairs(list) do
      result, _G[var] = pcall(require, path)
      catch_exception(result, path)
   end
end

local ini = inicfg.load({
   GENERAL = {
      dist = 1200
   }
}, thisScript().name .. ".ini")

local draw_dist_real = ffi.cast("float *", 0x00B7C4F0)
ffi.cast("unsigned char *", 0x005609FF)[0] = 0xEB
ffi.cast("unsigned char *", 0x00561344)[0] = 0xEB

local function set_fog_dist(param)
   if param > 3600.0 or param < 0 then 
      return false 
   end
   draw_dist_real[0] = param
   ini.GENERAL.dist = param
   return true
end

local function cmd_execute(param)
   param = tonumber(param)
   if param and set_fog_dist(param) then 
      inicfg.save(ini, thisScript().name .. ".ini")
      local format = "��������� ���������� �����������, ����� ��������: {FB4343}%d"
      log(string.format(format, param), -1)
   else 
      log("������� �������� �� {FB4343}0{FFFFFF} �� {FB4343}3600", -1) 
   end
end

function main()
   do
      if not doesDirectoryExist("moonloader\\config") then 
         createDirectory("moonloader\\config") 
      end
      if not doesFileExist(thisScript().name .. ".ini") then 
         inicfg.save(ini, thisScript().name .. ".ini") 
      end
   end

   while not isSampAvailable() do wait(0) end

   sampRegisterChatCommand("fogdist", cmd_execute)

   set_fog_dist(ini.GENERAL.dist)

   while true do wait(0)
      if draw_dist_real[0] ~= ini.GENERAL.dist then 
         set_fog_dist(ini.GENERAL.dist) 
      end
   end
end