script_name("SensFix")
script_dependencies("mimgui", "tabler_icons")

local BIND = 0x2E -- VK_DEL

local function default()
   -- default settings table
   return {
      general = {
         default = 0.71653,
         aiming = 0.400000,
         sniper = 0.400000,
      }
   }
end

local memory = require("memory")
local inicfg = require("inicfg")
local imgui  = require("mimgui")
local ti     = require("tabler_icons")

local ini = inicfg.load(default(), thisScript().name .. ".ini")

local win_state = imgui.new.bool(false)
-- imgui window state

local function write(float)
	memory.setfloat(0xB6EC1C, float / 1000, false)
	memory.setfloat(0xB6EC18, float / 1000, false)
end

function main()
	do -- save config
      if not doesDirectoryExist("moonloader\\config") then 
         createDirectory("moonloader\\config") 
      end

      if not doesFileExist(thisScript().name .. ".ini") then 
         inicfg.save(ini, thisScript().name .. ".ini") 
      end
   end

   imgui.to()

   while true do wait(0)
      do 
         local weapon = getCurrentCharWeapon(PLAYER_PED)
         local sens = ini.general.default[0]
         if isKeyDown(0x02) then
            if weapon == 34 then
               sens = ini.general.sniper[0]
            end -- sniper rifle
            if weapon >= 22 and weapon <= 33 then
               sens = ini.general.aiming[0]
            end -- normal guns
            if weapon >= 35 and weapon <= 38 then
               sens = ini.general.aiming[0]
            end -- bazooka and stuff
         end
         write(sens)
      end
      if isKeyJustPressed(BIND) then
         win_state[0] = not win_state[0]
      end
   end
end

local white  = imgui.ImVec4(1, 1, 1, 1)
local orange = imgui.ImVec4(1, 0.32, 0, 1)
local width = 150
local logo = "Sensitivity"

imgui.OnInitialize(function()
   local config = imgui.ImFontConfig()
   config.MergeMode = true
   config.PixelSnapH = true
   local iconRanges = imgui.new.ImWchar[3](ti.min_range, ti.max_range, 0)
   imgui.GetIO().Fonts:AddFontFromMemoryCompressedBase85TTF(ti.get_font_data_base85(), 15, config, iconRanges)
   imgui.GetIO().IniFilename = nil

   imgui.SwitchContext()
   local colors = imgui.GetStyle().Colors
   colors[imgui.Col.Text]                    = imgui.ImVec4(1.00, 1.00, 1.00, 1.00)
   colors[imgui.Col.TextDisabled]            = imgui.ImVec4(1.00, 1.00, 1.00, 0.20)
   colors[imgui.Col.SliderGrab]              = imgui.ImVec4(0.90, 0.90, 0.90, 1.00)
   colors[imgui.Col.SliderGrabActive]        = imgui.ImVec4(0.70, 0.70, 0.70, 1.00)
   colors[imgui.Col.ScrollbarBg]             = imgui.ImVec4(0.60, 0.60, 0.60, 0.90)
   colors[imgui.Col.ScrollbarGrab]           = imgui.ImVec4(0.90, 0.90, 0.90, 1.00)
   colors[imgui.Col.ScrollbarGrabHovered]    = imgui.ImVec4(0.80, 0.80, 0.80, 1.00)
   colors[imgui.Col.ScrollbarGrabActive]     = imgui.ImVec4(0.70, 0.70, 0.70, 1.00)
   colors[imgui.Col.FrameBg]                 = imgui.ImVec4(0.20, 0.20, 0.20, 1.00)
   colors[imgui.Col.FrameBgHovered]          = imgui.ImVec4(0.20, 0.20, 0.20, 0.80)
   colors[imgui.Col.FrameBgActive]           = imgui.ImVec4(0.20, 0.20, 0.20, 0.60)
   colors[imgui.Col.CheckMark]               = imgui.ImVec4(1.00, 1.00, 1.00, 1.00)
   colors[imgui.Col.Button]                  = imgui.ImVec4(0.20, 0.20, 0.20, 1.00)
   colors[imgui.Col.ButtonHovered]           = imgui.ImVec4(0.15, 0.15, 0.15, 1.00)
   colors[imgui.Col.ButtonActive]            = imgui.ImVec4(0.10, 0.10, 0.10, 1.00)
   colors[imgui.Col.TextSelectedBg]          = imgui.ImVec4(0.80, 0.80, 0.80, 0.80)
end)

local new_frame = imgui.OnFrame(function() return win_state[0] end,
function(player)
   imgui.SetNextWindowPos(imgui.ImVec2(40, 275), imgui.Cond.FirstUseEver)
   imgui.Begin(thisScript().name, nil, imgui.WindowFlags.NoResize + imgui.WindowFlags.NoTitleBar + imgui.WindowFlags.AlwaysAutoResize)
      do
         imgui.Link(thisScript().url, ti("brand-gitlab"), white, orange)
         imgui.SameLine((imgui.GetWindowSize().x - imgui.CalcTextSize(logo).x) / 2)
         imgui.Text(logo)
      end
      imgui.PushItemWidth(width)
      do
         imgui.InputFloat(" global", ini.general.default, 1)
         imgui.InputFloat(" aiming", ini.general.aiming, 1)
         imgui.InputFloat(" sniper", ini.general.sniper, 1)
         if imgui.Button("Save", imgui.ImVec2(width, 20)) then
            imgui.from()
            inicfg.save(ini, thisScript().name .. ".ini")
            imgui.to()
         end
      end
      imgui.PopItemWidth()
   imgui.End()
end)

--- looks like shit for me but when not much functionality it feels ok
imgui.to = function()
   local info = ini.general
   local new  = imgui.new

   info.default = new.float(info.default)
   info.aiming  = new.float(info.aiming)
   info.sniper  = new.float(info.sniper)
end

imgui.from = function()
   local info = ini.general

   info.default = info.default[0]
   info.aiming  = info.aiming[0]
   info.sniper  = info.sniper[0]
end
-- shitcode section end

-- clickable text
imgui.Link = function(link, name, color, color_hovered)
   local size = imgui.CalcTextSize(name)
   local p = imgui.GetCursorScreenPos()
   local p2 = imgui.GetCursorPos()
   local result_btn = imgui.InvisibleButton("##".. link .. name, size)

   if result_btn then
      os.execute("explorer ".. link)
   end

   imgui.SetCursorPos(p2)
   if imgui.IsItemHovered() then
      imgui.TextColored(color_hovered, name)
   else
      imgui.TextColored(color, name)
   end
   
   return result_btn
end