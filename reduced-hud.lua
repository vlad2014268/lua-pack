script_name("reduced-hud")
script_author("THERION")

local HEALTH_CHART_POSITION_X_POINTER_ADDRESS = 0x58EE87
local HEALTH_CHART_POSITION_Y_POINTER_ADDRESS = 0x58EE68
local HEALTH_CHART_SCALE_X_POINTER_ADDRESS = 0x5892D8
local HEALTH_CHART_SCALE_Y_POINTER_ADDRESS = 0x589358

local ARMOR_CHART_POSITION_X_POINTER_ADDRESS = 0x58EF59
local ARMOR_CHART_POSITION_Y_POINTER_ADDRESS = 0x58EF3A
local ARMOR_CHART_SCALE_X_POINTER_ADDRESS = 0x58915D
local ARMOR_CHART_SCALE_Y_POINTER_ADDRESS = 0x589146

local BREATH_CHART_POSITION_X_POINTER_ADDRESS = 0x58F11F
local BREATH_CHART_POSITION_Y_POINTER_ADDRESS = 0x58F100
local BREATH_CHART_SCALE_X_POINTER_ADDRESS = 0x589235
local BREATH_CHART_SCALE_Y_POINTER_ADDRESS = 0x58921E

local MONEY_TEXT_POSITION_X_POINTER_ADDRESS = 0x58F5FC
local MONEY_TEXT_POSITION_Y_POINTER_ADDRESS = 0x58F5DC
local MONEY_TEXT_SCALE_X_POINTER_ADDRESS = 0x58F564
local MONEY_TEXT_SCALE_Y_POINTER_ADDRESS = 0x58F54E

local WEAPON_POSITION_X_POINTER_ADDRESS = 0x58F5FC
local WEAPON_POSITION_Y_POINTER_ADDRESS = 0x58F5DC

local WEAPON_RENDER_ONE_XLU_SPRITE_CALL_ADDRESS = 0x58D8FD
local RENDER_ONE_XLU_SPRITE_PROTOTYPE = "void(__cdecl*)(float, float, float, float, float, unsigned __int8, unsigned __int8, unsigned __int8, __int16, float, char, char, char)"

local FIST_CSPRITE2D_DRAW_CALL_ADDRESS = 0x58D988
local CSPRITE2D_DRAW_PROTOTYPE = "void(__thiscall*)(void* this, CRect*, void*)"

local hooks = require("hooks")
local ffi = require("ffi")
ffi.cdef[[
   void *malloc(size_t size);
   void free(void *ptr);
   struct CRect { 
      float left;
      float top;
      float right;
      float bottom;
   };
   typedef struct CRect CRect;
]]

local function allocate_float(intial_value)
   local float_pointer = ffi.cast('float*', ffi.C.malloc(4))
   float_pointer[0] = intial_value
   return float_pointer
end

local function redirect_float(pointer_address, new_float_pointer)
   ffi.cast('float**', pointer_address)[0] = new_float_pointer
end

local function deallocate(cdata, initial_pointer)
   ffi.C.free(cdata)
end

local health_chart_position_x = allocate_float(120.0)
local health_chart_position_y = allocate_float(20.0)
local health_chart_scale_x = allocate_float(80.0)
local health_chart_scale_y = allocate_float(8.0)

local armor_chart_position_x = allocate_float(56.1)
local armor_chart_position_y = allocate_float(24.0)
local armor_chart_scale_x = allocate_float(45.5)
local armor_chart_scale_y = allocate_float(8.0)

local breath_chart_position_x = allocate_float(56.1)
local breath_chart_position_y = allocate_float(38.0)
local breath_chart_scale_x = allocate_float(45.5)
local breath_chart_scale_y = allocate_float(8.0)

local money_text_position_x = allocate_float(11.0)
local money_text_position_y = allocate_float(54.0)
local money_text_scale_x = allocate_float(0.45)
local money_text_scale_y = allocate_float(1)

function main()
   redirect_float(HEALTH_CHART_POSITION_X_POINTER_ADDRESS, health_chart_position_x)
   redirect_float(HEALTH_CHART_POSITION_Y_POINTER_ADDRESS, health_chart_position_y)
   redirect_float(HEALTH_CHART_SCALE_X_POINTER_ADDRESS, health_chart_scale_x)
   redirect_float(HEALTH_CHART_SCALE_Y_POINTER_ADDRESS, health_chart_scale_y)

   redirect_float(ARMOR_CHART_POSITION_X_POINTER_ADDRESS, armor_chart_position_x)
   redirect_float(ARMOR_CHART_POSITION_Y_POINTER_ADDRESS, armor_chart_position_y)
   redirect_float(ARMOR_CHART_SCALE_X_POINTER_ADDRESS, armor_chart_scale_x)
   redirect_float(ARMOR_CHART_SCALE_Y_POINTER_ADDRESS, armor_chart_scale_y)

   redirect_float(BREATH_CHART_POSITION_X_POINTER_ADDRESS, breath_chart_position_x)
   redirect_float(BREATH_CHART_POSITION_Y_POINTER_ADDRESS, breath_chart_position_y)
   redirect_float(BREATH_CHART_SCALE_X_POINTER_ADDRESS, breath_chart_scale_x)
   redirect_float(BREATH_CHART_SCALE_Y_POINTER_ADDRESS, breath_chart_scale_y)

   redirect_float(MONEY_TEXT_POSITION_X_POINTER_ADDRESS, money_text_position_x)
   redirect_float(MONEY_TEXT_POSITION_Y_POINTER_ADDRESS, money_text_position_y)
   redirect_float(MONEY_TEXT_SCALE_X_POINTER_ADDRESS, money_text_scale_x)
   redirect_float(MONEY_TEXT_SCALE_Y_POINTER_ADDRESS, money_text_scale_y)

   local resolutionX, resolutionY = getScreenResolution()
   local weaponIconX, weaponIconY = 30, 30

   draw_weapon_original = hooks.call.new(
      RENDER_ONE_XLU_SPRITE_PROTOTYPE,
      function(right, bottom, v1, width, height, v2, v3, v4, v5, v6, v7, v8, v9)
         right = (right - width) + ((resolutionX / 640.0) * weaponIconX * 0.5)
	      bottom = (bottom - height) + ((resolutionY / 448.0) * weaponIconY * 0.5)
   	   width = (resolutionX / 640.0) * weaponIconX * 0.5
	      height = (resolutionY / 448.0) * weaponIconY * 0.5
         
         return draw_weapon_original(right, bottom, v1, width, height, v2, v3, v4, v5, v6, v7, v8, v9)
      end,
      WEAPON_RENDER_ONE_XLU_SPRITE_CALL_ADDRESS
   )

   draw_fist_original = hooks.call.new(
      CSPRITE2D_DRAW_PROTOTYPE,
      function(_this, rect, color)
         rect.right = rect.left + (resolutionX / 640.0) * weaponIconX
         rect.top = rect.bottom + (resolutionY / 448.0) * weaponIconY

         return draw_fist_original(_this, rect, color)
      end,
      FIST_CSPRITE2D_DRAW_CALL_ADDRESS
   )
   wait(-1)
end
