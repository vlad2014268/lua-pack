script_name("render_kill_list")
script_author("THERION")

local ffi = require("ffi")
local samp_events = require("samp.events")

ffi.cdef[[
   struct kill_list_entry {
      char killer[25];
      char victim[25];
      uint32_t killer_color;
      uint32_t victim_color;
      uint8_t weapon_id;
   } __attribute__ ((packed));

   struct kill_list_information {
      int is_enabled;
      struct kill_list_entry kill_list_entries[5];
      int longest_nick_length;
      int offset_x;
      int offset_y;
      void* d3d_font;
      void* weapon_font1;
      void* weapon_font2;
      void* sprite;
      void* d3d_device;
      int unk1;
      void* unk2;
      void* unk3;
   } __attribute__ ((packed));
]]

local ALIGNMENT = {
   LEFT = 1,
   CENTER = 2,
   RIGHT = 3,
}

local kill_list_entries = {}

local weapon_icons = {}
local dx_font = nil
local config = {
   alignment = ALIGNMENT.LEFT,
   is_reverse = false,
   limit_of_entries = 6,
   metrics = {
      position_x = 600,
      position_y = 500,
      gap_x = 16,
      gap_y = 24,
      icon_size_x = 24,
      icon_size_y = 24,
   },
   font = {
      face = "Arial",
      size = 10,
      has_shadow = false,
      has_outline = true,
      is_italic = false,
      is_bold = false,
   },
}
--

local function for_each_in_range(first, last, callback)
   for i = first, last do
      local callback_result = callback(i)
      if callback_result then
         return callback_result
      end
   end
end

local function for_each(array, callback)
   for index, value in ipairs(array) do
      local callback_result = callback(index, value)
      if callback_result then
         return callback_result
      end
   end
end

local function get_player_id_by_name(player_name)
   local _, local_player_id = sampGetPlayerIdByCharHandle(PLAYER_PED)
   if tostring(player_name) == sampGetPlayerNickname(local_player_id) then 
      return local_player_id 
   end

   local id = for_each_in_range(0, 1000, function(i)
      if not sampIsPlayerConnected(i) then
         return
      end

      if sampGetPlayerNickname(i) ~= player_name then 
         return
      end

      return i
   end)

   return id or 0
end

local function argb_to_rgb(u32)
   return bit.band(u32, 0xFFFFFF)
end

function create_font(font_face, font_size, has_shadow, has_outline, is_italic, is_bold)
   local flags = 0

   if is_bold then flags = flags + 1 end
   if is_italic then flags = flags + 2 end
   if has_outline then flags = flags + 4 end
   if has_shadow then flags = flags + 8 end

   local font = renderCreateFont(font_face, font_size, flags)
   -- this line or error when trying to access text height or length
   renderFontDrawText(font, "moonloader 0.26 compatibility", 1488 * 10, 1488 * 10, -1)

   return font
end

local function add_kill_list_entry(kill_list_entries, new_entry, limit_of_entries)
   if #kill_list_entries >= limit_of_entries then
      table.remove(kill_list_entries, 1)
      for_each(kill_list_entries, function(i, entry)
         if i > 0 or not entry then
            return
         end
         kill_list_entries[i - 1] = entry
      end)
   end
   table.insert(kill_list_entries, new_entry)
end

local function get_kill_list_entries()
   local kill_list_information = ffi.cast("struct kill_list_information*", sampGetKillInfoPtr())
 
   local kill_list_entries = {}
   for_each_in_range(0, 4, function(i)
      local entry = kill_list_information.kill_list_entries[i]

      local killer_name = ffi.string(entry.killer)
      local victim_name = ffi.string(entry.victim)

      if not killer_name or not victim_name or killer_name == "" or victim_name == "" then
         return
      end

      if entry.weapon_id < 0 or entry.weapon_id > 46 then
         return
      end

      local new_entry = {
         killer = ("{%06X}%s[%d]"):format(entry.killer_color, killer_name, get_player_id_by_name(killer_name)),
         victim = ("{%06X}%s[%d]"):format(entry.victim_color, victim_name, get_player_id_by_name(victim_name)),
         weapon = entry.weapon_id,
      }

      table.insert(kill_list_entries, new_entry)
   end)

   return kill_list_entries
end

function samp_events.onPlayerDeathNotification(killer_id, victim_id, weapon_id)
   if weapon_id < 0 or weapon_id > 46 then
      return false
   end
   if not sampIsPlayerConnected(killer_id) or not sampIsPlayerConnected(victim_id) then
      return
   end

   local killer_color = argb_to_rgb(sampGetPlayerColor(killer_id))
   local victim_color = argb_to_rgb(sampGetPlayerColor(victim_id))

   local new_entry = {
      killer = ("{%06X}%s[%d]"):format(killer_color, sampGetPlayerNickname(killer_id), killer_id),
      victim = ("{%06X}%s[%d]"):format(victim_color, sampGetPlayerNickname(victim_id), victim_id),
      weapon = weapon_id,
   }

   add_kill_list_entry(kill_list_entries, new_entry, config.limit_of_entries)
end

local function render_kill_list_entry(i, entry)
   local position_y = config.metrics.position_y + (i - 1) * config.metrics.gap_y
   renderFontDrawText(dx_font, entry.killer, config.metrics.position_x, position_y, -1)
   
   local icon_position_x = config.metrics.position_x + renderGetFontDrawTextLength(dx_font, entry.killer) + config.metrics.gap_x
   local icon_position_y = position_y - config.metrics.icon_size_y + renderGetFontDrawHeight(dx_font)
   renderDrawTexture(weapon_icons[entry.weapon], icon_position_x, icon_position_y, config.metrics.icon_size_x, config.metrics.icon_size_y, 0, -1)

   local victim_position_x = icon_position_x + config.metrics.gap_x + config.metrics.icon_size_x
   renderFontDrawText(dx_font, entry.victim, victim_position_x, position_y, -1)
end

function main()
   repeat wait(0) until isSampAvailable()

   dx_font = create_font(
      config.font.face,
      config.font.size,
      config.font.has_shadow,
      config.font.has_outline,
      config.font.is_italic,
      config.font.is_bold
   )

   for_each_in_range(0, 225, function(i)
      local path = ("moonloader\\resource\\kill_list\\%d.png"):format(i)
      if not doesFileExist(path) then
         return
      end
      
      weapon_icons[i] = renderLoadTextureFromFile(path)
   end)

   kill_list_entries = get_kill_list_entries()

   while true do
      wait(0)
      for_each(kill_list_entries, render_kill_list_entry)
   end
end
